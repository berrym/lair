from setuptools import setup

setup(
    name='The Lair',
    version='0.0.1',
    packages=['lairchat', 'lairchat.cli', 'lairchat.gui', 'lairchat.crypto'],
    url='https://bitbucket.org/berrym/lair',
    license='GPLv3',
    author='Michael Berry',
    author_email='trismegustis@gmail.com',
    description='A small and simple chat application written in Python 3',
    install_requires=['PyQt5', 'pycryptodomex']
)
