# The Lair

The Lair Chat App

## Description

A small chat application written in Python 3

## Getting started

* Clone the git repository from https://github.com/berrym/lair.git

* Install a recent version of Python, tested with 3.6+

* A requirements.txt is provided in the repository so required packages can be installed using pip.
    * pip install --user requirements.txt

### Executing program

The main script can be executed from the command line, e.g.

* python3 lair.py

## Help

python3 lair.py --help

## Authors

Copyright 2020
Michael Berry <trismegustis@gmail.com>

## Version History

* 0.1.0
    * Initial Release

## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

See the COPYING file included in the git repository.
